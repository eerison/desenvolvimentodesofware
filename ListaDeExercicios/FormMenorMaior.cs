﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaDeExercicios
{
    public partial class FormMenorMaior : Form
    {
        public FormMenorMaior()
        {
            InitializeComponent();
        }

        private void buttonCalcular_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.MenorValor(textBoxNumeros.Text, radioButtonMenor.Checked, radioButtonMaior.Checked).ToString());
        }

        private int MenorValor(string numeros, bool checkedMenor, bool ckeckedMaior)
        {
            //cria variáveis para armazernar o menor numero eo valor do foreach que é string
            int numeroEscolhido = 0, elementInt = 0, count = 0;

            foreach (string element in numeros.Split(';'))
            {
                elementInt = int.Parse(element);
                //atribui ao menorValor, o primeiro elemento do vetor.
                if (count == 0)
                {
                    numeroEscolhido = elementInt;
                    count++;
                }

                if ((elementInt < numeroEscolhido && checkedMenor) || (elementInt > numeroEscolhido && ckeckedMaior))
                    numeroEscolhido = elementInt;
            }

            return numeroEscolhido;
        }
    }
}
