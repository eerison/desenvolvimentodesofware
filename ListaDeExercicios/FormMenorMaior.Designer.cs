﻿namespace ListaDeExercicios
{
    partial class FormMenorMaior
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxNumeros = new System.Windows.Forms.TextBox();
            this.buttonCalcular = new System.Windows.Forms.Button();
            this.radioButtonMenor = new System.Windows.Forms.RadioButton();
            this.radioButtonMaior = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxNumeros
            // 
            this.textBoxNumeros.Location = new System.Drawing.Point(61, 43);
            this.textBoxNumeros.Name = "textBoxNumeros";
            this.textBoxNumeros.Size = new System.Drawing.Size(176, 20);
            this.textBoxNumeros.TabIndex = 0;
            // 
            // buttonCalcular
            // 
            this.buttonCalcular.Location = new System.Drawing.Point(61, 91);
            this.buttonCalcular.Name = "buttonCalcular";
            this.buttonCalcular.Size = new System.Drawing.Size(176, 23);
            this.buttonCalcular.TabIndex = 1;
            this.buttonCalcular.Text = "Calcular";
            this.buttonCalcular.UseVisualStyleBackColor = true;
            this.buttonCalcular.Click += new System.EventHandler(this.buttonCalcular_Click);
            // 
            // radioButtonMenor
            // 
            this.radioButtonMenor.AutoSize = true;
            this.radioButtonMenor.Checked = true;
            this.radioButtonMenor.Location = new System.Drawing.Point(61, 68);
            this.radioButtonMenor.Name = "radioButtonMenor";
            this.radioButtonMenor.Size = new System.Drawing.Size(81, 17);
            this.radioButtonMenor.TabIndex = 2;
            this.radioButtonMenor.TabStop = true;
            this.radioButtonMenor.Text = "Menor valor";
            this.radioButtonMenor.UseVisualStyleBackColor = true;
            // 
            // radioButtonMaior
            // 
            this.radioButtonMaior.AutoSize = true;
            this.radioButtonMaior.Location = new System.Drawing.Point(160, 69);
            this.radioButtonMaior.Name = "radioButtonMaior";
            this.radioButtonMaior.Size = new System.Drawing.Size(77, 17);
            this.radioButtonMaior.TabIndex = 3;
            this.radioButtonMaior.Text = "Maior valor";
            this.radioButtonMaior.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lista de numeros (ex: 1;2;3;4)";
            // 
            // FormMenorMaior
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 145);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonMaior);
            this.Controls.Add(this.radioButtonMenor);
            this.Controls.Add(this.buttonCalcular);
            this.Controls.Add(this.textBoxNumeros);
            this.Name = "FormMenorMaior";
            this.Text = "FormMenorMaior";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumeros;
        private System.Windows.Forms.Button buttonCalcular;
        private System.Windows.Forms.RadioButton radioButtonMenor;
        private System.Windows.Forms.RadioButton radioButtonMaior;
        private System.Windows.Forms.Label label1;
    }
}