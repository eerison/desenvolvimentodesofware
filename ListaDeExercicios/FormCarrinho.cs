﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaDeExercicios
{
    public partial class FormCarrinho : Form
    {
        private int x = 13, y = 20;//valor eixo x e y inicial.
        public bool[] posicion = new bool[3];

        public FormCarrinho()
        {
            InitializeComponent();
            
        }

        private void buttonAddCarrinho_Click(object sender, EventArgs e)
        {
            if (!this.addProduto(textBox1.Text))
                MessageBox.Show("Limite máximo de produtos : " + this.posicion.Length);
        }

        public bool addProduto(string nome) {
            
            if (this.espacoLiberado()) {
                //Intância um objeto da classe GroupBox.
                GroupBox grupo = new GroupBox();
                grupo.Location = new Point(this.x,this.yPosicion());
                grupo.Size = new Size(250,30);
                

                //Istância um objeto da classe Label.
                Label nomeProduto = new Label();
                nomeProduto.Text = nome;
                nomeProduto.Location = new Point(10,10);
                //Adiciona label no GroupBox.
                grupo.Controls.Add(nomeProduto);

                //Istância um objeto da classe Button
                Button buttonDel = new Button();
                buttonDel.Text = "x";
                buttonDel.Name = this.indiceDisponivel();
                buttonDel.Location = new Point(230,8);
                buttonDel.Size = new Size(20,20);
                buttonDel.Click += new EventHandler(delProduto);
                //Adiciona button no GroupBox.
                grupo.Controls.Add(buttonDel);


                //Adiciona groupBox no formulario.
                groupBoxListaProdutos.Controls.Add(grupo);

                this.posicion[int.Parse(this.indiceDisponivel())] = true;
                return true;
            }

            return false;

        }

        private void delProduto(object sender, EventArgs e) {

            Button btn = sender as Button;
            int indice = int.Parse(btn.Name);
            groupBoxListaProdutos.Controls[indice].Dispose();

            this.posicion[indice] = false;
        }

        //Retorna valor do eixo y que esteja vago.
        private int yPosicion(){
            int margin = this.y;
            for (int i = 0; i < this.posicion.Length; i++)
                if (!this.posicion[i]) {
                    margin = this.y + i * 30;
                    break;
                }
            return margin;
        }

        //Verifica quais os indices estão disponiveis
        private string indiceDisponivel() {
            for (int i = 0; i < this.posicion.Length; i++)
                if (!this.posicion[i])
                    return i.ToString();
                return "Nenhum espaço liberado.";
        }

        //Verifica se existe algum espaço liberado.
        //caso tenha algum valor = false ele retorna verdadeiro!
        private bool espacoLiberado() {
            foreach (bool element in this.posicion)
                if (!element)
                    return true;
            return false;
        }


    }
}
