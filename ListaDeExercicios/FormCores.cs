﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaDeExercicios
{
    public partial class FormCores : Form
    {
        public FormCores()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("abrir caixa de cor?", "abrir", MessageBoxButtons.YesNo) == DialogResult.Yes)
                colorDialog1.ShowDialog();
        }
    }
}
