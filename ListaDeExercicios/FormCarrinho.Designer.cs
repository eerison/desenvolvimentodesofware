﻿namespace ListaDeExercicios
{
    partial class FormCarrinho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.buttonAddCarrinho = new System.Windows.Forms.Button();
            this.labelText = new System.Windows.Forms.Label();
            this.groupBoxListaProdutos = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 30);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(149, 20);
            this.textBox1.TabIndex = 0;
            // 
            // buttonAddCarrinho
            // 
            this.buttonAddCarrinho.Location = new System.Drawing.Point(168, 28);
            this.buttonAddCarrinho.Name = "buttonAddCarrinho";
            this.buttonAddCarrinho.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.buttonAddCarrinho.Size = new System.Drawing.Size(75, 23);
            this.buttonAddCarrinho.TabIndex = 1;
            this.buttonAddCarrinho.Text = "Add";
            this.buttonAddCarrinho.UseVisualStyleBackColor = true;
            this.buttonAddCarrinho.Click += new System.EventHandler(this.buttonAddCarrinho_Click);
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(13, 13);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(89, 13);
            this.labelText.TabIndex = 2;
            this.labelText.Text = "Nome do produto";
            // 
            // groupBoxListaProdutos
            // 
            this.groupBoxListaProdutos.AutoSize = true;
            this.groupBoxListaProdutos.Location = new System.Drawing.Point(14, 66);
            this.groupBoxListaProdutos.Name = "groupBoxListaProdutos";
            this.groupBoxListaProdutos.Size = new System.Drawing.Size(229, 52);
            this.groupBoxListaProdutos.TabIndex = 3;
            this.groupBoxListaProdutos.TabStop = false;
            this.groupBoxListaProdutos.Text = "Lista de Produtos";
            // 
            // FormCarrinho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.groupBoxListaProdutos);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.buttonAddCarrinho);
            this.Controls.Add(this.textBox1);
            this.Name = "FormCarrinho";
            this.Text = "FormCarrinho";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonAddCarrinho;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.GroupBox groupBoxListaProdutos;
    }
}