﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaDeExercicios
{
    public partial class FormMenu : Form
    {
        public FormMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Chama form carrinho
            new FormCarrinho().Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Chama form Menor valor
            new FormMenorMaior().Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Chama form listas
            new FormListas().Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Chama form cores
            new FormCores().Show();
        }   
    }
}
