﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ListaDeExercicios
{
    public partial class FormListas : Form
    {
        public FormListas()
        {
            InitializeComponent();
        }

        private void buttonGerar_Click(object sender, EventArgs e)
        {
            List<Cidades> cidades = new List<Cidades>();
            this.gerarCidades(int.Parse(textBoxQtd.Text), cidades);
            this.addComboBox(comboBoxLista, cidades);
        }

        public void gerarCidades(int qtd, List<Cidades> cidades){
            if(qtd > 0)
                for(int i = 1; i <= qtd; i++){
                    Cidades c = new Cidades();
                    c.id = i;
                    c.nome = "Cidade " + i;
                    cidades.Add(c);
                }

        }

        public void addComboBox(ComboBox comboBox, List<Cidades> cidades) { 
                comboBox.DisplayMember = "nome";
                comboBox.ValueMember = "id";
                comboBox.DataSource = cidades;
        }



    }
}
